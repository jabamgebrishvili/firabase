package com.example.registration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegistrationActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextRePassword: EditText
    private lateinit var buttonRegistration: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        init()

        registrationListeners()
    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextRePassword = findViewById(R.id.editTextRePassword)
        buttonRegistration = findViewById(R.id.buttonRegistration)
    }

    private fun registrationListeners(){
        buttonRegistration.setOnClickListener {

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val rePassword = editTextRePassword.text.toString()

            if (email.isEmpty() || password.isEmpty() || rePassword.isEmpty()) {
                Toast.makeText(this, "One of the input is empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.length < 9) {
                Toast.makeText(this, "The Password must consist of 9 characters!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (rePassword != password) {
                Toast.makeText(this, "The repeated password is incorrect!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { registration ->
                    if (registration.isSuccessful) {
                        Toast.makeText(this, "Congratulations!", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Something wrong! Please, try again", Toast.LENGTH_SHORT).show()
                        return@addOnCompleteListener
                    }
                }

        }

    }

}